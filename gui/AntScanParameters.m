function varargout = AntScanParameters(varargin)
% ANTSCANPARAMETERS MATLAB code for AntScanParameters.fig
%      ANTSCANPARAMETERS, by itself, creates a new ANTSCANPARAMETERS or raises the existing
%      singleton*.
%
%      H = ANTSCANPARAMETERS returns the handle to a new ANTSCANPARAMETERS or the handle to
%      the existing singleton*.
%
%      ANTSCANPARAMETERS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ANTSCANPARAMETERS.M with the given input arguments.
%
%      ANTSCANPARAMETERS('Property','Value',...) creates a new ANTSCANPARAMETERS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before AntScanParameters_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to AntScanParameters_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help AntScanParameters

% Last Modified by GUIDE v2.5 05-Aug-2012 14:39:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @scanparameters_OpeningFcn, ...
                   'gui_OutputFcn',  @scanparameters_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AntScanParameters is made visible.
function scanparameters_OpeningFcn(hObject, eventdata, handles, varargin)

    handles.output = hObject;

    global phandles;
    global SCAN;
    global PATHS;
    global fileName;
    global data;

    % Update handles structure
    guidata(hObject, handles);

    %globalize handles to take external calls from anothers gui
    phandles=handles;
    
    config=getPathSettings();
    SCAN    =config.defaultscan;
    PATHS   =config.settings;

    %DEFAULT SCAN VALUES
    %if particular file has specific values overwrite in that moment from
    %external call
    set(handles.BINSHISTOGRAM   ,'String',SCAN.BINSHISTOGRAM);
    set(handles.LASTFRAME       ,'String',SCAN.LASTFRAME);
    set(handles.FRAMES          ,'String',SCAN.LASTFRAME);
    set(handles.LASTFRAMESTEP   ,'String',SCAN.LASTFRAMESTEP);
    set(handles.MINCLUSTERSIZE  ,'String',SCAN.MINCLUSTERSIZE);
    set(handles.MAXCLUSTERSIZE  ,'String',SCAN.MAXCLUSTERSIZE);
    set(handles.ANTSPECIES      ,'String',SCAN.ANTSPECIES);
    set(handles.CONFIDENCE      ,'String',SCAN.CONFIDENCE);
    set(handles.fileName        ,'String',fileName);
        
    statsFile       = [PATHS.VIDEOPARAMETERS,fileName,'.stats.txt'];
    settingsFile    = [PATHS.VIDEOSETTINGS,fileName,'.js'];
    
    cla(handles.axes1);
    cla(handles.axes2);
    
     if(~isempty(fileName) && size(fileName,2)>1 && exist(statsFile,'file'))
         stats=loadjson(statsFile);
         stats=stats.stats;
         data=stats.data;

         if(isfield(stats,'BINSHISTOGRAM'))
            % load especific stats values
            set(handles.BINSHISTOGRAM   ,'String',stats.BINSHISTOGRAM);
            set(handles.LASTFRAME       ,'String',stats.LASTFRAME);
            set(handles.FRAMES          ,'String',stats.LASTFRAME);
            set(handles.LASTFRAMESTEP   ,'String',stats.LASTFRAMESTEP);
            set(handles.MINCLUSTERSIZE  ,'String',stats.MINCLUSTERSIZE);
            set(handles.MAXCLUSTERSIZE  ,'String',stats.MAXCLUSTERSIZE);
            set(handles.ANTSPECIES      ,'String',stats.ANTSPECIES);
            set(handles.CONFIDENCE      ,'String',stats.CONFIDENCE);
         end

         guivalues.MINCLUSTERSIZE    =str2double(get(handles.MINCLUSTERSIZE  ,'String'));
         guivalues.MAXCLUSTERSIZE    =str2double(get(handles.MAXCLUSTERSIZE  ,'String'));
         guivalues.BINSHISTOGRAM     =str2double(get(handles.BINSHISTOGRAM   ,'String'));

         cla(handles.axes1);
         cla(handles.axes2);
         xlim(handles.axes1,[guivalues.MINCLUSTERSIZE guivalues.MAXCLUSTERSIZE]);
         hist(handles.axes1,data,guivalues.BINSHISTOGRAM);

         reloadNormals_Callback(hObject, eventdata, handles)
     end
    
     if(~isempty(fileName) && size(fileName,2)>1 && exist(settingsFile,'file'))
        settings=loadjson(settingsFile);
        settings=settings.settings;
        set(handles.CURRENTLASTFRAME,'string',settings.frames(2));
        if(isfield(settings,'antvalues'))
            set(handles.ANTMIN  ,'String',settings.antvalues.min);
            set(handles.ANTMEAN ,'String',settings.antvalues.mean);
            set(handles.ANTMAX  ,'String',settings.antvalues.max);
        end
     end
         

% UIWAIT makes AntScanParameters wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = scanparameters_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function BINSHISTOGRAM_Callback(~, ~, ~)
% hObject    handle to BINSHISTOGRAM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of BINSHISTOGRAM as text
%        str2double(get(hObject,'String')) returns contents of BINSHISTOGRAM as a double


% --- Executes during object creation, after setting all properties.
function BINSHISTOGRAM_CreateFcn(hObject, ~, ~)
% hObject    handle to BINSHISTOGRAM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function MINCLUSTERSIZE_Callback(~, ~, ~)
% hObject    handle to MINCLUSTERSIZE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MINCLUSTERSIZE as text
%        str2double(get(hObject,'String')) returns contents of MINCLUSTERSIZE as a double


% --- Executes during object creation, after setting all properties.
function MINCLUSTERSIZE_CreateFcn(hObject, ~, ~)
% hObject    handle to MINCLUSTERSIZE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function MAXCLUSTERSIZE_Callback(~, ~, ~)
% hObject    handle to MAXCLUSTERSIZE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MAXCLUSTERSIZE as text
%        str2double(get(hObject,'String')) returns contents of MAXCLUSTERSIZE as a double


% --- Executes during object creation, after setting all properties.
function MAXCLUSTERSIZE_CreateFcn(hObject, ~, ~)
% hObject    handle to MAXCLUSTERSIZE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LASTFRAME_Callback(~, ~, ~)
% hObject    handle to LASTFRAME (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LASTFRAME as text
%        str2double(get(hObject,'String')) returns contents of LASTFRAME as a double


% --- Executes during object creation, after setting all properties.
function LASTFRAME_CreateFcn(hObject, ~, ~)
% hObject    handle to LASTFRAME (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function reloadHistogram_Callback(~, ~, handles)
% hObject    handle to reloadHistogram (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global video;
global settings;
global SCAN;
global data;

if(~exist('video','var') || isempty(video))
    % err = MException('ResultChk:BadInput', 'video is not open');
    % throw(err)
    % display some status message
    return;
end

guivalues.BINSHISTOGRAM     =str2double(get(handles.BINSHISTOGRAM   ,'String'));
guivalues.LASTFRAME         =str2double(get(handles.LASTFRAME       ,'String'));
guivalues.LASTFRAMESTEP     =str2double(get(handles.LASTFRAMESTEP   ,'String'));
guivalues.MINCLUSTERSIZE    =str2double(get(handles.MINCLUSTERSIZE  ,'String'));
guivalues.MAXCLUSTERSIZE    =str2double(get(handles.MAXCLUSTERSIZE  ,'String'));
    
if(settings.frames(2)< settings.frames(1)+ guivalues.LASTFRAME)
    disp('frame interval bad setting');
    return;
end

tic
   
    set(handles.FRAMES,'String',guivalues.LASTFRAME);

    env.local=settings;
    env.SCAN =SCAN;
    data=gatherData(video,env,guivalues);
    disp('recolectData process');
    

toc

% show histogram with new parameters
cla(handles.axes1);
xlim(handles.axes1,[guivalues.MINCLUSTERSIZE guivalues.MAXCLUSTERSIZE]);
[n,xout]=hist(data,SCAN.BINSHISTOGRAM);
bar(handles.axes1,xout,n);




function ANTSPECIES_Callback(hObject, eventdata, handles)
% hObject    handle to ANTSPECIES (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ANTSPECIES as text
%        str2double(get(hObject,'String')) returns contents of ANTSPECIES as a double


% --- Executes during object creation, after setting all properties.
function ANTSPECIES_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ANTSPECIES (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function reloadNormals_Callback(hObject, eventdata, handles)

global settings;
global SCAN;
global antvalues;
global normals;
global data;

    guivalues.ANTSPECIES        =str2double(get(handles.ANTSPECIES,'String'));
    guivalues.CONFIDENCE        =str2double(get(handles.CONFIDENCE,'String'));
    guivalues.MINCLUSTERSIZE    =str2double(get(handles.MINCLUSTERSIZE,'String'));
    guivalues.MAXCLUSTERSIZE    =str2double(get(handles.MAXCLUSTERSIZE,'String'));

    env.local=settings;
    env.SCAN =SCAN;

    if(exist('data','var') && ~isempty(data))
        [antvalues normals]=plotNormals(data,env,guivalues);
        
        set(handles.ANTMEAN         ,'String',antvalues.mean);
        set(handles.ANTMIN          ,'String',antvalues.min);
        set(handles.ANTMAX          ,'String',antvalues.max);
    end






function CONFIDENCE_Callback(hObject, eventdata, handles)
% hObject    handle to CONFIDENCE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of CONFIDENCE as text
%        str2double(get(hObject,'String')) returns contents of CONFIDENCE as a double


% --- Executes during object creation, after setting all properties.
function CONFIDENCE_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CONFIDENCE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in saveParameters.
function saveParameters_Callback(hObject, eventdata, handles)
% hObject    handle to saveParameters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global PATHS;
    global fileName;
    global data;
    global normals;
    global antvalues;
    global eventMgr;
        
    statsFile       = [PATHS.VIDEOPARAMETERS,fileName,'.stats.txt'];
    settingsFile    = [PATHS.VIDEOSETTINGS,fileName,'.js'];
    
    stats.ANTSPECIES        =str2double(get(handles.ANTSPECIES      ,'String'));
    stats.CONFIDENCE        =str2double(get(handles.CONFIDENCE      ,'String'));
    stats.MINCLUSTERSIZE    =str2double(get(handles.MINCLUSTERSIZE  ,'String'));
    stats.MAXCLUSTERSIZE    =str2double(get(handles.MAXCLUSTERSIZE  ,'String'));
    stats.BINSHISTOGRAM     =str2double(get(handles.BINSHISTOGRAM   ,'String'));
    stats.LASTFRAME         =str2double(get(handles.LASTFRAME       ,'String'));
    stats.LASTFRAMESTEP     =str2double(get(handles.LASTFRAMESTEP   ,'String'));

    antvalues={};
    antvalues.min = str2double(get(handles.ANTMIN  ,'String'));
    antvalues.mean = str2double(get(handles.ANTMEAN  ,'String'));
    antvalues.max = str2double(get(handles.ANTMAX  ,'String'));
    eventMgr.antSize = antvalues;

    stats.data      = data;
    stats.normals   = normals;

    stats=savejson('stats',stats);
    
    fd_stats=fopen(statsFile,'w');
    fprintf(fd_stats,stats);
    fclose(fd_stats);
    try
        settings=loadjson(settingsFile);
        settings=settings.settings;
        settings.antvalues=antvalues;
        settings=savejson('settings',settings);

        fd_settings=fopen(settingsFile,'w');
        fprintf(fd_settings,settings);
        fclose(fd_settings);

        display([settingsFile,' - saved']);
        display([statsFile,' - saved']);
    catch EM
        disp('Settings file does not exist');
    end
        
    


function LASTFRAMESTEP_Callback(hObject, eventdata, handles)
% hObject    handle to LASTFRAMESTEP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LASTFRAMESTEP as text
%        str2double(get(hObject,'String')) returns contents of LASTFRAMESTEP as a double


% --- Executes during object creation, after setting all properties.
function LASTFRAMESTEP_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LASTFRAMESTEP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ok.
function ok_Callback(hObject, eventdata, handles)
% hObject    handle to ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_3_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function ANTMIN_Callback(hObject, eventdata, handles)
% hObject    handle to ANTMIN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ANTMIN as text
%        str2double(get(hObject,'String')) returns contents of ANTMIN as a double


% --- Executes during object creation, after setting all properties.
function ANTMIN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ANTMIN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ANTMEAN_Callback(hObject, eventdata, handles)
% hObject    handle to ANTMEAN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ANTMEAN as text
%        str2double(get(hObject,'String')) returns contents of ANTMEAN as a double


% --- Executes during object creation, after setting all properties.
function ANTMEAN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ANTMEAN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ANTMAX_Callback(hObject, eventdata, handles)
% hObject    handle to ANTMAX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ANTMAX as text
%        str2double(get(hObject,'String')) returns contents of ANTMAX as a double


% --- Executes during object creation, after setting all properties.
function ANTMAX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ANTMAX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
