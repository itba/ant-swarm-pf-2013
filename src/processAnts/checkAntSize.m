% Execute statistical studies to learn ant size if never happend before

function checkAntSize(video,env,fileName,handles)
    PATHS=env.paths;    
    
    filesettings=strcat(PATHS.VIDEOSETTINGS,fileName,'.js');
    
    if(exist(filesettings,'file'))
        config=loadjson(strcat(PATHS.VIDEOSETTINGS,fileName,'.js'));
       
        if(isfield(config,'settings'))
            settings=config.settings;
            if(~isfield(settings,'antvalues'))
                statusFeedback(handles.status,'Learning Ant Size ...');

                environment.local=settings;            
                %environment.fileName=fileName;
                %environment.PATHS=env.paths;
                environment.SCAN=env.scan;

                [antvalues stats]=getHistogram(video,true,environment);
                settings.antvalues=antvalues;

                json=savejson('settings',settings);
                stats=savejson('stats',stats);

                % save histogram data
                %dlmwrite([env.paths.VIDEOPARAMETERS,fileName,'.stats.txt'],statsjson);

                if(strcmp(fileName,'')==0)
                    fd=fopen(strcat(PATHS.VIDEOSETTINGS,fileName,'.js'),'w');
                    fprintf(fd,json);
                    fclose(fd);

                    fd=fopen([env.paths.VIDEOPARAMETERS,fileName,'.stats.txt'],'w');
                    fprintf(fd,stats);
                    fclose(fd);
                    display(strcat(fileName,' - Settings saved'));
                end
                statusFeedback(handles.status,'Process finished');
            else
                statusFeedback(handles.status,'Ant size already studied');
            end
        end
    end
end

