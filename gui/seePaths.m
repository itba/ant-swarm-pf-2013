function varargout = seePaths(varargin)
% SEEPATHS MATLAB code for seePaths.fig
%      SEEPATHS, by itself, creates a new SEEPATHS or raises the existing
%      singleton*.
%
%      H = SEEPATHS returns the handle to a new SEEPATHS or the handle to
%      the existing singleton*.
%
%      SEEPATHS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SEEPATHS.M with the given input arguments.
%
%      SEEPATHS('Property','Value',...) creates a new SEEPATHS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before seePaths_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to seePaths_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help seePaths

% Last Modified by GUIDE v2.5 23-Jul-2012 20:34:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @seePaths_OpeningFcn, ...
                   'gui_OutputFcn',  @seePaths_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before seePaths is made visible.
function seePaths_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to seePaths (see VARARGIN)

% Choose default command line output for seePaths
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes seePaths wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = seePaths_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in LOADPATHS.
function LOADPATHS_Callback(hObject, eventdata, handles)
% hObject    handle to LOADPATHS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global paths;

paths=load('output\videoProcess\paths\P2DIL1-4N52.avi.path.txt');
disp('donde');



% --- Executes on button press in PLAY.
function PLAY_Callback(hObject, eventdata, handles)
% hObject    handle to PLAY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global paths;

    WIDTH=720;
    HEIGHT=1280;
    
    board=ones(WIDTH,HEIGHT);
    
    for i=1:size(paths,1)
        board(round(paths(i,3)),round(paths(i,4)))=0;
        imshow(board,'Parent',handles.axes1);
    end
