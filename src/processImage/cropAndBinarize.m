function [frame, bw]=cropAndBinarize(frame,rect,regionMask)
    WHITE = 255;
      
    if(size(rect,2)>0)
        frame=imcrop(frame,rect);
    end

    if(size(regionMask,1)>0)
        % select white to outside regionMask
        R=frame(:,:,1);
        G=frame(:,:,2);
        B=frame(:,:,3);

        R(~regionMask)=WHITE;
        G(~regionMask)=WHITE;
        B(~regionMask)=WHITE;

        frame=cat(3,R,G,B);
    end
%     tic
    bw=umbralizar(frame);
%     disp('umbra')
%     toc
    
end
