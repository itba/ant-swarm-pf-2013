function printMass( fullPathfilename, massArray)
try
current = pwd;
fidMasa = fopen(strcat(fullPathfilename),'w+');
cd(current);

print = [nonzeros(massArray(:,3)),nonzeros(massArray(:,2)),...
    nonzeros(massArray(:,1))];

[c, ~] = size(print);

for i=1 : c
    fprintf(fidMasa, '%d, %d, %d\r\n', print(i, 3), print(i,2),print(i, 1));
end

fclose(fidMasa);
catch EM
    % usear cancel
end