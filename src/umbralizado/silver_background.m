function [answer] = silver_background( img )
    se = strel('disk',3);
    img = imsubtract(imadd(img,imtophat(img,se)), imbothat(img,se));
    r=img(:,:,1);
    rev=255-r;
    % regularizar la iluminación
    i2=imtophat(rev,strel('disk',30));
    % equalizar
    i3=imadjust(i2);
    % umbralizar con otsu
    threshold=graythresh(i3);
    i4=im2bw(i3,threshold);
    % primer filtro 
    target= i4>0;
    i5=img;
    img(target)=255;
    % segundo filtro con rojo
    target=find(img(:,:,1)==255 & img(:,:,2)<85 & img(:,:,3)<70);
    i5(target)=255;

    answer = zeros(size(i4));
    answer(target) = 1;
    answer = ~answer;
end