function ans = getMaxXY(mask)
%GETMAXXY 
%   Devuelve (x,y) del vertice superior izquierdo y 
%   (x,y) del vertice inferior derecho.

[rows cols]  = size(mask);

firstY = 0;
lastY = 0;
firstX = 0;
lastX = 0;

for i=1: rows
    for j=1: cols
        value = mask(i, j);
        if(value==1)
            if(firstY==0)
                firstY = i;    
            end
            lastY = i;
        end
    end
    
end

for i=1: cols
    for j=1:rows
        value = mask(j,i);
        if(value==1)
            if(firstX==0)
                firstX=i;
            end
            lastX=i;
        end
    end
end

ans = [firstX firstY lastX lastY];

end

