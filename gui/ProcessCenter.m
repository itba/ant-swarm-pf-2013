function varargout = ProcessCenter(varargin)
% PROCESSCENTER MATLAB code for ProcessCenter.fig
%      PROCESSCENTER, by itself, creates a new PROCESSCENTER or raises the existing
%      singleton*.
%
%      H = PROCESSCENTER returns the handle to a new PROCESSCENTER or the handle to
%      the existing singleton*.
%
%      PROCESSCENTER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROCESSCENTER.M with the given input arguments.
%
%      PROCESSCENTER('Property','Value',...) creates a new PROCESSCENTER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ProcessCenter_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ProcessCenter_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ProcessCenter

% Last Modified by GUIDE v2.5 28-Feb-2012 14:18:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ProcessCenter_OpeningFcn, ...
                   'gui_OutputFcn',  @ProcessCenter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ProcessCenter is made visible.
function ProcessCenter_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ProcessCenter (see VARARGIN)

% Choose default command line output for ProcessCenter
handles.output = hObject;

global PATHS;

PATHS=getPathSettings();
PATHS=PATHS.settings;

initializeGUI(handles);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ProcessCenter wait for user response (see UIRESUME)
% uiwait(handles.figure1);

function message(handles,text)
    set(handles.status,'string',text);
    
function a=isReady(filename)
global PATHS;

if(exist(strcat(PATHS.VIDEOPROCESS,filename,'-Masa.txt'),'file') ...
   || exist(strcat(PATHS.VIDEOPROCESS,filename,'-CentroMasa.txt'),'file'))
    a='DONE';
else if(exist(strcat(PATHS.VIDEOSETTINGS,filename,'.js'),'file'))
        a='READY';
    else
        a='-';
    end
end

        

function initializeGUI(handles)
    
    global dataRows;
    global PATHS;
    
    I=imread(strcat(PATHS.IMAGES,'ant.png'));
    axes(handles.axes1);
    imshow(I);

    dirs=dir(strcat(PATHS.VIDEOFILES,'*avi'));
    dataRows=numel(dirs);
    
    if (numel(dirs)==0)
        message(handles,'No videos or bad directory');
    else
        message(handles,'');
        data=cell(1,4);
        for i=1: numel(dirs)
            data(i,:)={dirs(i).name,isReady(dirs(i).name),false,false};
        end
        
        set(handles.uitable,'data',data);
    end





% --- Outputs from this function are returned to the command line.
function varargout = ProcessCenter_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in analyze.
function analyze_Callback(~, ~, handles)
% hObject    handle to analyze (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global dataRows;
    global mainOpenVideo;
    global mainGUIhandles;
    global mainPlayButton;
    global mainAskDirectory;
    global PATHS;
    global externalCloseGUI;
    mainAskDirectory=0;
    
    data=get(handles.uitable,'data');
    
    mainGUI=mainWindow();
    
    for i=1: dataRows
        a=data(i,3);
        state=data(i,2);
        state=state{1};
        if (a{1} && (strcmp(state,'READY')))
            filename=data(i,1);
            %filename{1};
            feval(mainOpenVideo,filename{1},PATHS.VIDEOFILES,mainGUIhandles,1);
            feval(mainPlayButton,'','',mainGUIhandles,0);
        end
    end
    feval(externalCloseGUI,'',''); % close main GUI
    
    
function gui=mainWindow()
 % get the main_gui handle (access to the gui)
 mainGUIhandle       = antSwarm;       
 %processHandle       = ProcessCenter;       
% get the data from the gui (all handles inside gui_main)
 gui         = guidata(mainGUIhandle);
 %processData         = guidata(processHandle);
 
% change gui strings
%param1=get(mainGUIdata.edit1, 'String');
%param2=get(mainGUIdata.edit2, 'String')
%param3=get(mainGUIdata.edit3, 'String');
 
% save changed data back into main_gui
%guidata(main_gui, mainGUIdata);
%guidata(gui);
%guidata(processData);

%set(handles.text23,'String', param1);
%set(handles.text24,'String', param2);
%set(handles.text25,'String', param3);


% --- Executes on button press in seeResults.
function seeResults_Callback(~, ~, ~)
% hObject    handle to seeResults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
