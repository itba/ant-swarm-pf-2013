function showFrame(handles, value, video, rect, regionMask)
    try
        frame = read(video,value);
        if(size(rect, 2)>0)
            [frame,bw]=cropAndBinarize(frame,rect,regionMask);
            frame=paintAnts(frame, regionMask, bw);
            if(get(handles.seeOtsu, 'value')>0)
                imshow(bw,'InitialMagnification', 50,'Parent',handles.axes8);
            end
        end 
        imshow(frame,'InitialMagnification', 50,'Parent',handles.axes3);
        set(handles.currentFrame,'String',value);
        set(handles.slider, 'Value', fix(value));
    catch EM
       % do nothing frame out of video 
       %disp('index out of video');
    end
end