function [qAnt swarm antBallProp]=countAnts(bw,antvalues,handles)

    MAX_ANT = antvalues.max;
    MEAN_ANT = antvalues.mean;
    MIN_ANT = antvalues.min;

    L=bwconncomp(~bw);
    prop=regionprops(L,'Area','Centroid','BoundingBox','PixelList');
    swarm=struct('Area',{},'Centroid',{},'BoundingBox',{},'PixelList',{});
    ants=find([prop.Area]> MIN_ANT & [prop.Area]< MAX_ANT);

    for i=1: size(ants,2)
        swarm(i,1)=prop(ants(i));
        %swarm(i,1).Centroid=round(prop(ants(i)).Centroid);
        swarm(i,1).Centroid=(prop(ants(i)).Centroid);
    end

    qAnt = size(swarm,1);

    antBall=find([prop.Area]>=MAX_ANT);
    
    nqAnt=0;

    if(qAnt>0)
        CURRENT_MEAN_ANT=mean([prop(ants).Area]);
        %disp(['Mean ',num2str(CURRENT_MEAN_ANT)]);
    else
        CURRENT_MEAN_ANT=MEAN_ANT;    
        %disp(['Main mean ',num2str(CURRENT_MEAN_ANT)]);
    end
    CURRENT_MEAN_ANT=MEAN_ANT;

    for i=1: size(antBall,2)

        %disp(['ants: ',num2str(prop(antBall(i)).Area/CURRENT_MEAN_ANT)]);
        iAnts=round(prop(antBall(i)).Area/CURRENT_MEAN_ANT);
        % TODO deber?a divir por el promedio de tama?os de las hormigas que
        % estan en este frame ME PARECE (Jose)

        if(iAnts < 2)
            %msg=['W: Ball less than 1 Ant ', num2str(prop(antBall(i)).Area)];
            %statusFeedback(handles.status,msg);
            iAnts=1;
        end

        % Fuzzy c means - parameters
        % options(1) exponent for the partition matrix
        % options(2) max iterations
        % options(3) minimum amount of improvement
        % options(4) info display during the iteration

        fcmOptions=[2,20,1e-3,0];
        olist=blurMass(prop(antBall(i)).PixelList);
        [center, ~, ~] = fcm(olist,iAnts,fcmOptions);

        for j=1:size(center,1)
            nqAnt=nqAnt+1;
            index=qAnt+nqAnt;
            swarm(index,1).Area=CURRENT_MEAN_ANT; 
            % TODO - area average comentar en el informe que la salida muy
            % probablemente sea continua y no discreta, pe. 539.48 en vez de
            % 540
            %swarm(index,1).Centroid=[round(center(j,1)),round(center(j,2))];
            swarm(index,1).Centroid=[(center(j,1)),(center(j,2))];
            % default initialization TODO: Comentar en el informe
            swarm(index,1).BoundingBox=prop(antBall(i)).BoundingBox;
            swarm(index,1).PixelList=prop(antBall(i)).PixelList;
            %antBallProp.CountAnts = iAnts;
        end

    end

     antBallProp.ball = prop(antBall);
     antBallProp.iAnt = round([prop(antBall).Area]/CURRENT_MEAN_ANT);
     antBallProp.iAnt = antBallProp.iAnt';
   % antBallProp={};
    
    for k=1:size(swarm,1)
        swarm(k).id=0;
        swarm(k).tracked=0;
    end

    qAnt=qAnt+nqAnt;



function olist=blurMass(ilist)
    % TODO comentar en el informe sobre el blur de la lista de pixeles
    f=size(ilist,1);
    THRESHOLD=0.01;
    r=rand(1,f);
    olist=ilist(r>THRESHOLD,:);
    %disp(['ilist ', num2str(size(ilist,1)),' olist ', num2str(size(olist,1))]);
    
    
