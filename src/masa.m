function [answer] = masa( logicalImage )
%MASA Summary of this function goes here
%   return 'solidity' property

    %aux = +logicalImage;
    %answer = regionprops(aux, 'Solidity');
    aux = ~logicalImage;
    answer = round(sum(sum(aux)));
end

