%Para ver las diferencias grande que hay en las masas.
%enviar carpeta del video, carpeta del archivo de masas, nombre del file
%sin extencion.
function getImageErrorMass(folderVideo, folderMass, name, percent, doPlot)

    filename = strcat(folderMass, name, '.avi-Masa.txt');
    
    data = importdata(filename);
    
    framesData = data(:,1);
    massData = data(:,2);
    
    if(doPlot)
        plot(framesData, massData);
    end
        
    videoFile = strcat(folderVideo, name, '.avi');
    
    video = VideoReader(videoFile);
    
    initialFrame = framesData(1);
    prevMass = massData(1);
    initialFrame=initialFrame+1;
    endFrame = length(massData);
    
    for i=2:endFrame
        if( abs(prevMass-massData(i))>percent )
            
            fileNameImg=strcat(filename,i, int2str(framesData(i)),'.png');
            if(exist(fileNameImg, 'file')==0)
                img = read(video,framesData(i));
                imwrite(img,fileNameImg);
                fprintf(strcat('SAVE FILE ', fileNameImg, ' \n'));
            end
            fileNameImg=strcat(filename, int2str(framesData(i-1)),'.png');
            if(exist(fileNameImg, 'file')==0)
                img = read(video,framesData(i-1));
                imwrite(img,fileNameImg);
                fprintf(strcat('SAVE FILE ', fileNameImg, ' \n'));
                pause(.5);
            end
        end
        prevMass = massData(i);
    end
end

%/Users/Dani/Desktop/Todos_Avi_Bien/P2CONCENTRADO_1.avi
%/Users/Dani/Desktop/PF2012/2012-pf-hormigas/output/videoProcess/P2CONCENTRADO_1.avi-Masa.txt