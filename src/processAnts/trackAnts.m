
function [swarm lostant antBallProp,qAnt]=trackAnts(selected,bw,dna,handles,currentFrame,antvalues)
    % selected ants - selected ants with region prop and id already assigned
    % bw            - current otsu frame
    % dna           - detect new ants flag. Append new ants to swarm
    global stop;
    global maxID;
    
    [qAnt, regions, antBallProp]=countAnts(bw,antvalues,handles);
    MAX_INT=intmax('uint32');

%   ANT_PROXIMITY=round(0.3*antvalues.mean);    % TODO comentar en el informe experimental value
    ANT_PROXIMITY=13;    % TODO comentar en el informe experimental value    
  %  disp(['size selected ',num2str(size(selected,2))]);
    
    swarm   =cell(1,round(size(regions,1)));  % current track ants        % selected * 3 ojo si es cero
    lostant =cell(1,round(size(regions,1)));  % lost or collision ant set % mismo tama?o de inicializaci?n
    
    aof=0;       % ant out of frame ant
    %maxID=0;     % aux for dna
    amatch=0;    % ant match
    
    % 1) search for nearest region ant
    
    for j=1:size(selected,2)        
        min = MAX_INT; % distance between 2 ants
        region={};
        id=0;
        index=1;
        for i=1:size(regions,1)
            % ecuclidean distance
            distance=norm(selected{j}.region.Centroid-regions(i).Centroid);
            
            if( isfield(regions(i),'tracked') && ~regions(i).tracked ...
                    && distance < ANT_PROXIMITY && distance < min)
                min     = distance;             
                region  = regions(i);          % potential region
                id      = selected{j}.id;      % potential id
                index=i;
            end
            
            if( ~isfield(regions(i),'tracked') )
                % it should never happend
                disp('no tracked 1');
                plot(handles.axes3,regions(i).Centroid(1),regions(i).Centroid(2),'b*')
                regions(i)
                stop=true;
            end
            
        end
        
        if(min==MAX_INT )
            % 2) ant out of frame 
            aof=aof+1;
            lostant{aof}=selected{j}; % {id,path,region}
            msg=['Ant out of frame ID: ',num2str(selected{j}.id)];
            statusFeedback(handles.status,msg);
        else
            % 3) ANT MATCH
            regions(index).tracked=1;
            k=size(selected{j}.path,2);
            amatch=amatch+1;
            %disp(['id: ',num2str(id),' k:',num2str(k),' Area:',num2str(region.Area)]);
            
            swarm{j-aof}.path       = selected{j}.path;         % historic path
            %swarm{j-aof}.path{k+1}  = round(region.Centroid);   % current position
            swarm{j-aof}.path{k+1}  = (region.Centroid);   % current position
            swarm{j-aof}.region     = region;                   % region
            swarm{j-aof}.id         = id;                       % id
            swarm{j-aof}.firstFrame = selected{j}.firstFrame;   % historic path
            
            if(maxID<id)
                maxID=id;
            end
        end
        
    end
    
    k= amatch;       % swarm index
    
    if(dna)
        
        %4.1) if no ant selected track everything
        if(maxID==0)
            for i=1:size(regions,1)
                regions(i).tracked  = 1;
                amatch              = amatch+1;
%                swarm{i}.path{1}    = round(regions(i).Centroid);   % current position
                swarm{i}.path{1}    = (regions(i).Centroid);   % current position
                swarm{i}.region     = regions(i);                   % region
                swarm{i}.id         = i;                        % id
                swarm{i}.firstFrame = currentFrame;   % historic path
            end
            maxID=amatch+1;
        end
        
        sizeSwarm=amatch;
        % 4) add new ant to swarm from current region
        
        for i=1:size(regions,1)
             % find untracked ants out of swarm struct
             if( isfield(regions(i),'tracked') && ~regions(i).tracked)
                 sameAnt=0;
                 for j=1:sizeSwarm
                     if(regions(i).Centroid==swarm{j}.region.Centroid) % equals Ant condition 
                         sameAnt=1;                   % already tracked ant
                         break;                       
                     end
                 end
                 if(~sameAnt)
                     regions(i).tracked=1;
                     amatch=amatch+1;
                     maxID=maxID+1;                         % new max id
                     k=k+1;                                 % swarm index
                     swarm{k}.region=regions(i);            % NEW ANT.region
                     swarm{k}.path={};
                     swarm{k}.path{1}=regions(i).Centroid;  % NEW ANT.path
                     swarm{k}.id=maxID;                     % NEW ANT.id
                     swarm{k}.firstFrame=currentFrame;
                 end
             end
             if( ~isfield(regions(i),'tracked') )
                % it should never happend
                disp('no tracked 2')
                plot(handles.axes3,regions(i).Centroid(1),regions(i).Centroid(2),'y*')
                regions(i)
                stop=true;
            end
        end
    end
   
    % Shrink ant sets
    % keep braces warning
    
    if(aof>0) 
        lostant={lostant{:,1:aof}};
    else
        lostant={};
    end
    if(amatch>0)
        swarm={swarm{:,1:amatch}};
    else
        swarm={};
    end
    
end






