function clearMask( handles )

    global regionMask;
    global video;
    global rect;
    global eventMgr;
    
    regionMask=[];
    rect=[];
    showFrame(handles, str2double(get(handles.currentFrame, 'String')) , video, rect, regionMask)
    eventMgr.antSize = {};
end

