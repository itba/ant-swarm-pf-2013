function bw = substract_with_empty( img, THRESHOLD )

    global emptyFrame;

    if(~isempty(emptyFrame))
        b=img;
        c=emptyFrame-b;
        %c=imtophat(c,strel('disk',30));
        ants=c>THRESHOLD;
        bw=ones(size(c))*255;
        bw(ants)=0;
        bw=im2bw(bw);
    else
        bw=im2bw(img);
    end

end