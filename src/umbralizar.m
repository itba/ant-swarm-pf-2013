function bw = umbralizar(img)
    global comboFunc;

    val = get(comboFunc,'Value');
    string_list = get(comboFunc,'String');
    selected_string = string_list{val}; % Convert from cell array
                                        % to string
    %selected_string ='white_acrilic_background';
    bw = feval(selected_string, img);
end