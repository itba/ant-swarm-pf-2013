classdef eventManager < handle
    properties (SetObservable, GetObservable, AbortSet)
      PropOne;
      antSize;
    end
    methods
        function obj = eventManager()
         %obj.PropOne{end+1} = val(1);
         %addlistener(obj,'PropOne','PreGet',@obj.getPropEvt);
         %addlistener(obj,'PropOne','PreSet',@obj.setPropEvt);
         addlistener(obj,'antSize','PostSet',@obj.setPropEvt);
        end
        function propval = get.PropOne(obj)
         %disp('get.PropOne called')
         propval = obj.PropOne;
        end
        function set.PropOne(obj,val)
         %disp('set.PropOne called')
         obj.PropOne = val;
        end
      
        function set.antSize(obj,val)
            obj.antSize = val;
        end
      
        %function getPropEvt(obj,src,evnt)
         %disp ('Pre-get event triggered')
        %cend
        function setPropEvt(obj,~,~)
            if ~isempty(obj.antSize)
                set(obj.PropOne.pushbutton6, 'enable', 'on'); %#ok<*MCSUP>
                set(obj.PropOne.pushbutton7, 'enable', 'on');
                set(obj.PropOne.antBallsCheckbox, 'enable', 'on');
                set(obj.PropOne.boundingBoxesCheck, 'enable', 'on');
                set(obj.PropOne.showantpath, 'enable', 'on');
                set(obj.PropOne.dnaCheck, 'enable', 'on');
                set(obj.PropOne.savePathAnt, 'enable', 'on');
            else
                set(obj.PropOne.pushbutton6, 'enable', 'off');
                set(obj.PropOne.pushbutton7, 'enable', 'off');
                set(obj.PropOne.antBallsCheckbox, 'enable', 'off');
                set(obj.PropOne.boundingBoxesCheck, 'enable', 'off');
                set(obj.PropOne.showantpath, 'enable', 'off');
                set(obj.PropOne.dnaCheck, 'enable', 'off');
                set(obj.PropOne.savePathAnt, 'enable', 'off');
            end
        end
    end  
end

