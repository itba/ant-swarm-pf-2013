function varargout = antSwarm(varargin)
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @antSwarm_OpeningFcn, ...
                       'gui_OutputFcn',  @antSwarm_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT


function antSwarm_OpeningFcn(hObject, ~, handles, varargin)
    % Choose default command line output for antSwarm
    handles.output = hObject;

    global PATHS;
    global SCAN_PARAMETERS;
    global messages;
    global MAX_BUFFER_MESSAGES;
    
    MAX_BUFFER_MESSAGES = 18;
    messages=cell(1,MAX_BUFFER_MESSAGES);
    
    
    % LOAD APPLICACTION GENERAL PATH

    config=getPathSettings();
    PATHS=config.settings;
    SCAN_PARAMETERS=config.defaultscan;

    I=imread(strcat(PATHS.IMAGES,'lupaB.png'));
    %I=imread(strcat(PATHS.IMAGES,'lupe_linepithema.jpg'));
    imshow(I,'Parent',handles.axes7);
    

    clearBufferMask();

    global seeOtsu;
    global otsuPercent;

    global frameShowStep;
    global frameOtsuProcess;
    global lastOtsu;
    global mainGUIhandles;
    %global massStruct;
    global massArray;
    global centerMassStruct;
    global antvalues;
    global rect;
    global eventMgr;
    global antHasSelected;
    % ESTO ES PARA QUE OTRA GUI LLAME A FUNCIONES DE ESTA GUI

    global mainOpenVideo;
    global mainPlayButton;
    global mainAskDirectory;
    global externalCloseGUI;
    global comboFunc;
    
    antHasSelected=false;
    
    mainGUIhandles=handles;

    frameOtsuProcess=1;
    frameShowStep=1;
    
    eventMgr = eventManager();

    %massStruct={};
    massArray=[];
    centerMassStruct={};
    antvalues={};

    seeOtsu=false;
    otsuPercent=0.4;
    lastOtsu=0;
    rect = [];
    %set(handles.otsuPercentSlider,'value',otsuPercent);
    %set(handles.percentOtsu,'String',otsuPercent);
    set(handles.showOtsuProcessLabel,'String',frameOtsuProcess);
    set(handles.frameShowStepLabel,'String',frameShowStep);

    
    mainOpenVideo= @openVideoWrap;
    mainPlayButton=@pushbutton6_Callback;
    externalCloseGUI=@closeGUI;
    mainAskDirectory=1;
    comboFunc = handles.comboFunc;
    
    files = dir('./src/umbralizado');
    
    %struct = cell(1, length(files)-2);
    i=1;
    struct=cell(1,length(files));
    for n = 1:length(files)
        if( ~isempty(files(n).name) && ~(files(n).name(1)=='.') )
            struct{i} = strrep(files(n).name, '.m', '');
            i=i+1;
        end
    end
    if (i==1)
        i=2;
    end
    struct=struct(1,1:i-1);
    set(comboFunc, 'String', struct);
    
    set(handles.comboSpecies, 'String',config.species.NAMES);
    
    % Update handles structure
    set(handles.figure1,'CloseRequestFcn',@closeGUI);
    guidata(hObject, handles);


function clearBufferMask()
    global regionMask;
    global lastcleanimage;

    lastcleanimage=[];
    regionMask=[];

function varargout = antSwarm_OutputFcn(~, ~, handles) 
    varargout{1} = handles.output;


% --- Executes on button press in Open Video.
function openVideo(~, ~, handles)
    global fileName;
    global pathName;
    global PATHS;
    global SCAN_PARAMETERS;
    global video;

    [fileName,pathName,~] =uigetfile(strcat(PATHS.VIDEOFILES,'*.avi'), ...
        'Select a Video File');
    
     try
        openVideoWrap(fileName,pathName,handles,0);
        set(handles.fileName,'String',['File ',fileName]);
        environment.paths=PATHS;
        environment.scan=SCAN_PARAMETERS; 
        checkAntSize(video,environment,fileName,handles);
        set(handles.USESLIDE,'visible','on');
        set(handles.INSPECTTOPLAY,'visible','on');
     catch ME
         statusFeedback(handles.status,'Cancel open video',0);
     end
    
    
    
    
function openVideoWrap(fileNameW,pathNameW,handles,exCall)
    global fileName;
    global pathName;
    global video;
    global cantFrames;
    global stop;
    global frames;
    global duration;
    global PATHS;
    global ants;
    global eventMgr;
    global settings;
    global emptyFrame;
    global rect;
    global regionMask;
    global externalCall;
    
    if(~isempty(exCall))
        externalCall=exCall;    
    else
        externalCall=0;
    end
    
    cla(handles.axes3,'reset');
    cla(handles.axes8,'reset');
    set(handles.comboSpecies,'Value',1);
    CLEANCONSOLE_Callback(0, 0, handles);
    fileName=fileNameW;
    filename=[PATHS.VIDEOFRAME,fileName,'empty.png'];
    
    emptyFrame={};
    
     if(exist(filename,'file') )
        emptyFrame=imread(filename);
        statusFeedback(handles.status,'Empty frame availabe');
    end
    
    settings={};
    ants={};
    
    fileName=fileNameW;
    pathName=pathNameW;
    
    stop=true;
    clearBufferMask();

    video = VideoReader((strcat(pathName,fileName)));
    
    frames = get(video, 'FrameRate');
    duration = get(video, 'Duration');
    cantFrames = frames*duration;
    cantFrames = round(cantFrames-1);

    set(handles.text16,'String',0);% centor de masa
    set(handles.text17,'String',0);% masa total
    
    set(handles.text18,'String',cantFrames);
    set(handles.lastFrame,'String',cantFrames);
    set(handles.firstFrame,'String',1);
    set(handles.slider,'Min',1);
    set(handles.slider,'value',1);
    set(handles.slider,'Max', cantFrames);
    
    set(handles.currentFrame,           'String', '1'); % first frame
    set(handles.selectTargetRegion,     'Enable', 'on');
    set(handles.selectPoints,           'Enable', 'on');
    set(handles.pushbutton4,            'Enable', 'on');
    set(handles.pushbutton5,            'Enable', 'on');
    set(handles.pushbutton6,            'Enable', 'on');
    set(handles.pushbutton7,            'Enable', 'on');
    set(handles.pushbutton8,            'Enable', 'on');
    set(handles.pushbutton9,            'Enable', 'on');
    set(handles.clearMask,              'Enable', 'on');
    set(handles.clearAntButton,         'Enable', 'on');
    set(handles.showantpath,            'Enable', 'on');
    set(handles.dnaCheck,               'Enable', 'on');
    set(handles.boundingBoxesCheck,     'Enable', 'on');
    set(handles.OutputAntPath,          'Enable', 'on');
    set(handles.slider,                 'Enable', 'on');
    set(handles.centroMasa,             'Enable', 'on');
    set(handles.masa,                   'Enable', 'on');
    set(handles.saveFrame,              'Enable', 'on');
    set(handles.seeOtsu,                'Enable', 'on');
    set(handles.showantpath,            'Enable', 'on');
    set(handles.antBallsCheckbox,       'Enable', 'on');
    set(handles.antUnit,                'visible','off');
    
    eventMgr.PropOne = handles;
    
    eventMgr.antSize = {};
    
    filesettings=strcat(PATHS.VIDEOSETTINGS,fileName,'.js');
    
    if(exist(filesettings,'file'))
        chargeSettings(filesettings,handles);
    else
          
        rect={};
        regionMask={};
        showFrame(handles, 1, video, [], []);
    end

     

function chargeSettings(fileSettings,handles)

    global regionMask;
    global frameShowStep;
    global frameOtsuProcess;
    global rect;
    global seeOtsu;
    global zone;
    global framesize;
    global antvalues;
    global settings;
    global cantFrames;
    global video;
    global lastcleanimage;
    global eventMgr;
    global PATHS;
        
    file=loadjson(fileSettings);
    
    settings={};
    rect={};
    regionMask={};
    
    if(isfield(file,'settings'))
        settings=file.settings;
        framesize=settings.framesize;

        if(isfield(settings,'binaryFnc'))
            binaryFnc=settings.binaryFnc;
            options = get(handles.comboFunc,'String');
            for i=1:size(options)
                if(strcmp(options{i},binaryFnc))
                    set(handles.comboFunc,'Value',i);
                    break;
                end
            end
        end
        
        if(isfield(settings,'nameSpecies'))
            name=settings.nameSpecies;
            options = get(handles.comboSpecies,'String');
            for i=1:size(options)
                if(strcmp(options{i},name))
                    set(handles.comboSpecies,'Value',i);
%                     if(i==1)
%                         I=imread(strcat(PATHS.IMAGES,'species/','default.jpg'));    
%                     else
%                         I=imread([PATHS.IMAGES,'species/',name,'.jpg']);    
%                     end            
% 
%                     imshow(I,'Parent',handles.axes8);                    
                     break;
                end
            end
            

        end

        zone=settings.regionMask;
        regionMask=roipoly(framesize(1),framesize(2),zone(:,1),zone(:,2));

        rect=settings.rect;

        if(isfield(settings,'antvalues'))
            antvalues=settings.antvalues;
            eventMgr.antSize = antvalues;
        else
            antvalues={};
            eventMgr.antSize = {};
        end

        regionMask = imcrop(regionMask, rect);
        frameShowStep=settings.processOpt(1);
        frameOtsuProcess=settings.processOpt(2);
        seeOtsu=settings.outfileOpt(3);
        
        if(size(settings.outfileOpt,2)>4)
            antPaths            =settings.outfileOpt(5);
            dnaCheck            =settings.processOpt(3);
            showantpath         =settings.processOpt(4);
            boundingBoxesCheck  =settings.processOpt(5);

            set(handles.OutputAntPath, 'value',antPaths);
            set(handles.dnaCheck,'value',dnaCheck);
            set(handles.showantpath,'value',showantpath);
            set(handles.boundingBoxesCheck,'value',boundingBoxesCheck);
        end
        
        if(settings.frames(2)<=cantFrames)
            % Last frame value
            set(handles.text18,'string',settings.frames(2));    
            cantFrames=settings.frames(2);
        end

        set(handles.firstFrame,'string',settings.frames(1));
        set(handles.currentFrame,'string',settings.frames(3));
        set(handles.slider,'value',settings.frames(3));

        set(handles.centroMasa,'value',settings.outfileOpt(1));
        set(handles.masa,'value',settings.outfileOpt(2));
        set(handles.seeOtsu,'value',settings.outfileOpt(3));
        [~, l] = size(settings.outfileOpt);
        if(l>3)
            set(handles.antBallsCheckbox,'value',settings.outfileOpt(4));
        end

        set(handles.frameShowStep,'value',settings.processOpt(1));
        set(handles.frameOtsuProcess,'value',settings.processOpt(2));

        set(handles.frameShowStepLabel,'string',settings.processOpt(1));
        set(handles.showOtsuProcessLabel,'string',settings.processOpt(2));

        strings = get(handles.comboFunc, 'String');
        index = 1:length(strings);

        if(isfield(settings,'comboFunc'))
            value = strcmp(strings, settings.comboFunc);
            if(value>0)
                set(handles.comboFunc, 'Value', index(value));
            end
        end
        
        if(size(rect,2)>0 && size(regionMask,2)>0 && settings.frames(3)<= cantFrames)
            frame = read(video,settings.frames(3));
            [frame, bw]=cropAndBinarize(frame,rect,regionMask);    
            frame=paintAnts(frame, regionMask, bw);
            imshow(frame,'Parent',handles.axes3);
            lastcleanimage=frame;
        end

    else
        statusFeedback(handles.status,'Invalid file setting',0);
    end

 
function selectTargetRegion_Callback(~, ~, handles)
     global regionMask;
     global zone;
     global rect;
     global framesize;
     global eventMgr;
     global settings;
     
     % Clear last frame with old region mask
     %if(size(rect,2)>0)
        clearMask(handles);   
     %end
     
     actualImage=getimage(handles.axes3);
     
     try
        % Unico axes que se llama s?lo;
        axes(handles.axes3);
       	[regionMask x y]=roipoly(actualImage); % this is a logical matrix
        if ~isempty(regionMask)
            zone=[x,y];
            framesize=size(regionMask);
            result = getMaxXY(regionMask);
            ancho = result(3) - result(1);
            alto = result(4) - result(2);
            rect = [result(1),result(2),ancho, alto];
            regionMask = imcrop(regionMask, rect);

            [actualImage, bw]=cropAndBinarize(actualImage,rect,regionMask);    
            actualImage=paintAnts(actualImage, regionMask, bw);

            imshow(actualImage,'Parent',handles.axes3);
            if(isfield(settings,'antvalues'))
                eventMgr.antSize = settings.antvalues;
            end
            statusFeedback(handles.status,'region mask selected',0);
        end
     catch EM
         statusFeedback(handles.status,'Cancel region mask selector',0);
%          disp('linea 363');
%          disp(EM);
     end
     
     
    
function clearAnt()
    global ants;
    ants={};
 
function selectPoints_Callback(~, ~, handles)
    %global otsuPercent;
    global ants;
    global lastAnts;
    global antvalues;
    global bw;
    global antHasSelected;
    global regionMask;
    
    frame=str2double(get(handles.currentFrame,'String'));       % get image from main axes
    if(size(regionMask,2)>0)
        if(~isempty(antvalues))
            ants=selectAnts(bw,ants,frame,antvalues, handles);      % select some ants from main frame
            lastAnts={};                                            % erase lostAnts
            if(size(ants,2)>0)
                antHasSelected=1;
            end
            statusFeedback(handles.status,[num2str(size(ants,2)),' ant/s already selected']);
        else
            statusFeedback(handles.status,'(!) First inspect ants');
        end
    else
        statusFeedback(handles.status,'(!) First select a region to study');
    end
    

 
function slider_Callback(hObject, ~, handles)
    global video;
    global rect;
    global regionMask;
    
    try
        value=get(hObject,'Value');
        % PRINT FRAME
        value=ceil(value);

        showFrame(handles, value, video, rect, regionMask)
    catch EM
        % do anything
    end
        



function slider_CreateFcn(hObject, ~, ~)
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end

%Callback del boton '<' de first frame
function pushbutton4_Callback(~, ~, handles)
    value=get(handles.currentFrame,'String');
    set(handles.firstFrame,'String',value);

%Callback del boton '>' de last frame
function pushbutton5_Callback(~, ~, handles)
    global cantFrames;
    
    value=get(handles.currentFrame,'String');
    %set(handles.lastFrame,'String',value);
    set(handles.text18,'String',value);
    cantFrames=str2double(value);
    

%Callback del boton Play
function pushbutton6_Callback(~, ~, handles,mainAsk)
    startFrame = str2num(get(handles.currentFrame, 'String')); %#ok<ST2NM>
    set(handles.USESLIDE,'visible','off');
    set(handles.INSPECTTOPLAY,'visible','off');
    
    global cantFrames;
    global stop;
    global FileNameMasa;
    global FileNameCentroMasa;
    global FileNameAntBalls;
    global fileName;
    global antBalls;
    global antBalls_index;
    global mainAskDirectory;
    global PATHS;
    global lastAnts;
    global lastAnts_index;
    global fullPathFileNameMasa;
    global fullPathFileNameCentroMasa;
    global fullPathFileNameAntBalls;
    global centerMassStruct;
    global cm_index;
    global massArray;
    global seeOtsu;
    global maxID;
    
    global SAhandles;
    
    global hImage;
    global regionMask;
    
    if(isempty(regionMask))
        statusFeedback(handles.status,'(!) Select a region to study',0)
        return;
    end
    
    
    SAhandles=handles; % power metal umbralization 
   
    framesMalloc=cantFrames-startFrame+1;
    
    massArray=zeros(framesMalloc, 3);
    centerMassStruct=cell(1,framesMalloc);
    lastAnts=cell(1,framesMalloc);
    antBalls=cell(1,framesMalloc);
    antBalls_index  =0;
    lastAnts_index  =0;
    cm_index        =0;
    maxID           =0; % aux for dna    
    stop            =false;
    hImage={};
    
    if(exist('mainAsk','var') && mainAsk==0)
            mainAskDirectory=mainAsk;
    end

    %set(handles.status, 'Visible', 'off');

    if(get(handles.masa, 'Value')>0)
        current = pwd;
        outdata=PATHS.VIDEOPROCESS;
        if(mainAskDirectory)
            cd(outdata);
            [FileNameMasa,PathNameMasa,~] = uiputfile('*.txt','Guardar output de masa', strcat(fileName, '-Masa.txt'));
        else
            FileNameMasa=strcat(fileName, '-Masa.txt');
            %TODO
            PathNameMasa=outdata;
        end
        fullPathFileNameMasa = strcat(PathNameMasa,FileNameMasa);
        cd(current);
    end

    if(get(handles.centroMasa, 'Value')>0)
        current = pwd;
        outdata=PATHS.VIDEOPROCESS; 
        %repetead because we want another directory for center mass
        if(mainAskDirectory)
            cd(outdata);
            [FileNameCentroMasa,PathNameCentroMasa,~] = uiputfile('*.txt','Guardar output de centro de masa', strcat(fileName, '-CentroMasa.txt'));
        else
            FileNameCentroMasa=strcat(fileName, '-CentroMasa.txt');
            PathNameCentroMasa=outdata;
        end
        fullPathFileNameCentroMasa = strcat(PathNameCentroMasa,FileNameCentroMasa);
        cd(current);
    end

    if(get(handles.antBallsCheckbox, 'Value')>0)
        current = pwd;
        outdata=PATHS.VIDEOPROCESS;
        if(mainAskDirectory)
            cd(outdata);
            [FileNameAntBalls,PathNameAntBalls,~] = uiputfile('*.txt','Guardar output de Ants cluster', strcat(fileName, '-AntsCluster.txt'));
        else
            FileNameAntBalls=strcat(fileName, '-AntsCluster.txt');
            %TODO
            PathNameAntBalls=outdata;
        end
        fullPathFileNameAntBalls = strcat(PathNameAntBalls,FileNameAntBalls);
        cd(current);
    end
    
    
    
%      try
        if(~seeOtsu)
            set(handles.axes8,'visible','off');
        end
        dna=get(handles.dnaCheck,'value');
        fframe=str2double(get(handles.firstFrame,'string'));
        ttframe=cantFrames-fframe;
        
        
   
        
        cla(handles.axes3,'reset');
        axes(handles.axes3);
         hold on;
        for i=startFrame:cantFrames
            if(~stop)
                actualizaFrameValue(handles, i, fframe, ttframe);
%                 tic
                actualizaVideoFrame(handles, i, dna);
%                 disp('actualiza video frame');
%                 toc
            else
                statusFeedback(handles.status,'video stoped by user',0)
                break;
            end
        end

       
       
        
        %selected frames has finish completly
        if(str2double(get(handles.currentFrame,'String'))==cantFrames)
            statusFeedback(handles.status,'No more frames to analize',0);
        end
        %si termino el video sin que haga click en stop
        if(~stop)
            
            if(antBalls_index==0)
                antBalls_index=1;
            end
            if(cm_index==0)
                cm_index=1;
            end
            %SHRINK global cell arrays
         % is not neccesary to shrink massArray and antPath
        
            if( get(handles.centroMasa, 'Value')>0)
                centerMassStruct =centerMassStruct(1,1:cm_index);
                printCenterMass(fullPathFileNameCentroMasa, centerMassStruct);
            end
            if(get(handles.masa, 'Value')>0)
                printMass(fullPathFileNameMasa, massArray);
            end
            if(get(handles.antBallsCheckbox, 'Value')>0)
                antBalls         =antBalls(1,1:antBalls_index);
                printAntBalls(fullPathFileNameAntBalls, antBalls);
            end
            if(get(handles.OutputAntPath, 'Value')>0)
                savePathAnt_Callback(0, 0, 0);
            end
        end
%     catch EM
%       statusFeedback(handles.status,' - ',0);
%       disp('linea 568');
%       disp(EM.stack);
%       disp(EM);
%     end

function actualizaVideoFrame(handles, i,dna)
    global video;
    global centerMass;
    global Mass;
    global stop;
    global seeOtsu;
    global antBalls;
    global antBalls_index;
    global regionMask;
    global rect;
    global lastAnts;
    global lastAnts_index;
    global ants;
    global massArray;
    global centerMassStruct;
    global cm_index;
    global antvalues;
    global bw;
    global antHasSelected;
    global hImage;

    frame = read(video,i);
%     tic
    [frame, bw]=cropAndBinarize(frame,rect,regionMask);
    frame=paintAnts(frame, regionMask, bw);
%      disp('crop and paint');
%      toc
    cla(handles.axes3);
    
    if(~stop)
        centerMass = centroMasa(bw);
        Mass = masa(bw);
       
        if(dna || antHasSelected)
            % 1) track selected ants between frames
            if(~isempty(antvalues))
%                 tic
                [ants lost antBallProp qAnt]=trackAnts(ants,bw,dna,handles,i,antvalues);
%                 disp('trackAnts');
%                 toc
                set(handles.qAnt,'String',qAnt);
                set(handles.antUnit,'visible','on');
                %antBalls{1}{1} === FRAME
                %antBalls{1}{2}.Area === Area (o cualquier propiedad)
                if ~isempty(antBallProp)
                    antBalls_index=antBalls_index+1;
                    antBalls{antBalls_index} = {i  antBallProp};
                end
                if ~isempty(lost)
                    lastAnts_index=lastAnts_index+1;
                    lastAnts{lastAnts_index}=lost;
                end

                %set(handles.antTotalPath,'String',size(lastAnts,2)+size(ants,2))
                %CODIGO PARA MOSTRAR EL CENTRO DE MASA DE LAS
                %ANTBALLS
                %for j=1:size(antBalls, 2)
                %    for r=1:size(antBalls{j}{2},2)
                %      [point] = antBalls{j}{2}(r).Centroid;
                %      hold on
                %      plot(point(1), point(2), 'wo');
                %      hold off
                %    end   
                %end
            end
        end
        
        if(isempty(hImage))
            hImage=imshow(frame,'InitialMagnification',67,'Parent',handles.axes3 );
            set(hImage,'EraseMode','none');
            set(hImage,'HandleVisibility','off'); 
            % this is to be hidden of cla(handles.axes3)
        else
            set(hImage,'CData',frame);
        end
        
        if(seeOtsu)
            imshow(bw,'InitialMagnification', 50, 'Parent', handles.axes8);
%             hold on;
        end
               
        if(dna || antHasSelected)
            % 2) mark position with their current id number
            % TODO repetead UI function
            
            for n=1:size(ants,2)
                bc = ants{n}.region.Centroid;
                if(get(handles.boundingBoxesCheck, 'Value')>0)
                    rectangle('Position',ants{n}.region.BoundingBox,'EdgeColor','g','LineWidth',1, 'Parent',handles.axes3);
                    hold on;
                end
                c={'m','r','g','b','c','k'};
                if(get(handles.showantpath, 'Value')>0)
                    px=size(ants{n}.path,2);
                    py=size(ants{n}.path,2);
                    
                    for i=1:size(ants{n}.path,2)
                        point=ants{n}.path{i};
                        px(i)=point(1);
                        py(i)=point(2);
                    end
                    %plot(handles.axes3,px,py,'LineWidth',1,'Color',[0.6, 0.8, 0.6],'LineStyle','-');
                    plot(handles.axes3,px,py,'LineWidth',1,'Color',[1-n/size(ants,2), n/size(ants,2), 0.4],'LineStyle','-');
                    %plot(handles.axes3,px,py,'LineWidth',1,'Color',c{mod(ants{n}.id,size(c,2))+1},'LineStyle','-');
                    
                    hold on;
                end
                
                a=text(bc(1),bc(2),int2str(ants{n}.id));
                set(a, 'FontName', 'Arial', 'FontWeight', 'bold', 'FontSize', 8, 'Color', 'yellow','Parent',handles.axes3);
                hold on;
            end
        end
            
           set(handles.text16, 'String', sprintf('[ %.0f, %.0f ]', centerMass.Centroid(1), centerMass.Centroid(2)));
           set(handles.text17, 'String', [num2str(Mass),' px ']);
            plot(handles.axes3,centerMass.Centroid(1), centerMass.Centroid(2), 'wx');
            plot(handles.axes3,centerMass.Centroid(1), centerMass.Centroid(2), 'wo');

         if(get(handles.masa, 'Value')>0)
             if(~dna)
                 qAnt=0;
             end
             massArray(i,:) = [i, Mass, qAnt];
         end
         if(get(handles.centroMasa, 'Value')>0)
             cm_index=cm_index+1;
             centerMassStruct{cm_index} = {i centerMass.Centroid};
         end
         drawnow;    
    end
    
  
function actualizaFrameValue(handles, i,fframe, ttframe)
    set(handles.slider,         'Value', fix(i));
    set(handles.currentFrame,   'String', num2str(fix(i)));
    set(handles.PERCENT,        'String', [num2str(round((i-fframe)/ttframe*100)),'%']);

% do nothing !!    
function pushbutton6_ButtonDownFcn(~, ~, handles)
function pushbutton7_ButtonDownFcn(~, ~, handles)
function pushbutton1_ButtonDownFcn(~, ~, handles)
function selectTargetRegion_ButtonDownFcn(~, ~, handles)


%Callback del boton STOP
function pushbutton7_Callback(~, ~, handles)
    global stop;
    global antBalls;
    global fullPathFileNameMasa;
    global massArray;
    global fullPathFileNameCentroMasa;
    global centerMassStruct;
    global fullPathFileNameAntBalls;
    global antBalls_index;
    global cm_index;
    
    if(antBalls_index==0)
        antBalls_index=1;
    end
    if(cm_index==0)
        cm_index=1;
    end
    %SHRINK global cell arrays
    % is not neccesary to shrink massArray and antPath

    if( get(handles.centroMasa, 'Value')>0)
        centerMassStruct =centerMassStruct(1,1:cm_index);
        printCenterMass(fullPathFileNameCentroMasa, centerMassStruct);
    end
    if(get(handles.masa, 'Value')>0)
        printMass(fullPathFileNameMasa, massArray);
    end
    if(get(handles.antBallsCheckbox, 'Value')>0)
        antBalls         =antBalls(1,1:antBalls_index);
        printAntBalls(fullPathFileNameAntBalls, antBalls);
    end
    if(get(handles.OutputAntPath, 'Value')>0)
        savePathAnt_Callback(0, 0, 0);
    end

    stop=true;

%Callback del boton '<' del slider
function previus(~, ~, handles)
    
    global video;
    global rect;
    global regionMask;
    i = str2double(get(handles.currentFrame, 'String'));
    if i>1
        i=i-1;
        showFrame(handles, i, video, rect, regionMask);
        %actualizaFrameValue(handles, i);
        %actualizaVideoFrame(handles, i);
    end

%Callback del boton '>' del slider
function pushbutton9_Callback(~, ~, handles)
    global cantFrames;
    global video;
    global rect;
    global regionMask;
    i = str2double(get(handles.currentFrame, 'String'));
    %if i<round(cantFrames)-1
    if i<cantFrames
        i=i+1;
        showFrame(handles, i, video, rect, regionMask)
        %actualizaFrameValue(handles, i);
        %actualizaVideoFrame(handles, i);
    end




function closeGUI(~,~)
    %src is the handle of the object generating the callback (the source of the event)
    %evnt is the The event data structure (can be empty for some callbacks)
    
    global stop;
    global externalCall;
    
    if(~externalCall)
        selection = questdlg('Do you want to close the GUI?',...
                         'Close Request Function',...
                         'Yes','No','Yes');
        switch selection,
       case 'Yes',
        stop=true;
        delete(gcf);
       case 'No'
         return
        end
    else
        stop=true;
        delete(gcf);
    end
    

% --- Executes on button press in saveFrame.
function saveFrame_Callback(~, ~, handles)
    global PATHS;
    global rect;
    global video;
    global fileName;

    currentFrame=get(handles.currentFrame, 'String');
    
    showFrame(handles, str2double(currentFrame), video, rect, []);
    
    filename=[PATHS.VIDEOFRAME,fileName,'-',currentFrame,'-otsu','.png'];
    img=getimage(handles.axes8);
    if(~isempty(img))
        imwrite(img,filename);
    end
    filename=[PATHS.VIDEOFRAME,fileName,'-',currentFrame,'-main','.png'];
    img=getimage(handles.axes3);
    if(~isempty(img))
        imwrite(img,filename);
    end


% --- Executes on button press in clearMask.
function clearMask_Callback(~, ~, handles)
    global rect;
    rect=[];
    clearMask(handles);

% --- Executes on button press in seeOtsu.
function seeOtsu_Callback(~, ~, handles)
    global seeOtsu;
    seeOtsu=(get(handles.seeOtsu,'value')==1);


% --- Executes on slider movement.
function otsuPercentSlider_Callback(~, ~, handles)
    global otsuPercent;
    otsuPercent=get(handles.otsuPercentSlider,'value');
    set(handles.percentOtsu,'String',otsuPercent);


% --- Executes during object creation, after setting all properties.
function otsuPercentSlider_CreateFcn(hObject, ~, ~)
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end


% --- Executes on slider movement.
function frameOtsuProcess_Callback(~, ~, handles)
    global frameOtsuProcess;
    frameOtsuProcess=round(get(handles.frameOtsuProcess,'value'));
    set(handles.showOtsuProcessLabel,'String',frameOtsuProcess);



% --- Executes during object creation, after setting all properties.
function frameOtsuProcess_CreateFcn(hObject, ~, ~)
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end


% --- Executes on slider movement.
function frameShowStep_Callback(~, ~, handles)
    global frameShowStep;
    frameShowStep=round(get(handles.frameShowStep,'value'));
    set(handles.frameShowStepLabel,'String',frameShowStep);


% --- Executes during object creation, after setting all properties.
function frameShowStep_CreateFcn(hObject, ~, ~)
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end

function saveConfiguration_Callback(~, ~, handles)
    global fileName;
    global rect;
    global PATHS;
    global zone;
    global framesize;
    global antvalues;
    global eventMgr;
    global settings;
    global video;
    
    if(isempty(video))
        statusFeedback(handles.status,'(!) There is no open video',0);
    else
        if(isempty(zone)|| isempty(rect))
            statusFeedback(handles.status,'(!) No region study to save',0);
        else
            firstFrame=str2double(get(handles.firstFrame,'string'));
            lastFrame=str2double(get(handles.text18,'string'));
            currentFrame=str2double(get(handles.currentFrame,'string'));

            massCenter=get(handles.centroMasa,'value');
            mass=get(handles.masa,'value');
            antBalls=get(handles.antBallsCheckbox, 'value');
            antPaths=get(handles.OutputAntPath, 'value');
            seeOtsu=get(handles.seeOtsu,'value');
            
            dnaCheck            =get(handles.dnaCheck,'value');
            showantpath         =get(handles.showantpath,'value');
            boundingBoxesCheck  =get(handles.boundingBoxesCheck,'value');
            

            showStepLabel=str2double(get(handles.frameShowStepLabel,'string'));
            otsuProcessInterval=str2double(get(handles.showOtsuProcessLabel,'string'));

            val = get(handles.comboFunc,'Value');
            string_list = get(handles.comboFunc,'String');
            selected_string = string_list{val};

            op = get(handles.comboSpecies,'Value');
            options= get(handles.comboSpecies,'String');
            nameSpecies= options{op};
            
            settings={};
            settings.filename   =fileName;
            settings.regionMask =zone;
            settings.framesize  =framesize;
            settings.rect       =rect; 
            settings.frames     =[firstFrame,lastFrame,currentFrame];
            settings.outfileOpt =[massCenter,mass,seeOtsu,antBalls,antPaths];
            settings.processOpt =[showStepLabel,otsuProcessInterval,...
                                dnaCheck,showantpath,boundingBoxesCheck];
            settings.binaryFnc  =selected_string;
            settings.nameSpecies=nameSpecies;

            if(~isempty(antvalues))
                settings.antvalues  = antvalues;
                eventMgr.antSize = antvalues;
            end

            json=savejson('settings',settings);

            if(strcmp(fileName,'')==0)
                fd=fopen(strcat(PATHS.VIDEOSETTINGS,fileName,'.js'),'w');
                fprintf(fd,json);
                fclose(fd);
                statusFeedback(handles.status,strcat(fileName,' - Settings saved'),0);
            end
        end
        
    end



function clearAntButton_Callback(~, ~, ~)
    global antHasSelected;
    
    clearAnt();
    antHasSelected=0;

function savePathAnt_Callback(~, ~, ~)
    global lastAnts;
    global lastAnts_index;
    global ants;
    global PATHS;
    global fileName;

    % TODO sacar esto que se haga autom?tico
    lastAnts         =lastAnts(1,1:lastAnts_index);
    
    saveAntPath(PATHS.VIDEOPATHS,fileName,ants,lastAnts);



% --- Executes on button press in Inspect Species.
function pushbutton15_Callback(~, ~, handles)
 % Open Parameters GUI
    global settings;
    global regionMask;
    global video;
    
    if(isempty(video))
        statusFeedback(handles.status,'(!) There is no video to inspect',0);
    else
        if(isempty(regionMask))
           statusFeedback(handles.status, '(!) Select a region to study & save it', 0);
        else
            if(isempty(settings))
               statusFeedback(handles.status, '(!) First save configuration', 0);
            else
                mainGUIhandle       = AntScanParameters;       
                guidata(mainGUIhandle);       
            end 
        end
    end
    
 

% --- Executes on selection change in comboFunc.
function comboFunc_Callback(~, ~, handles)
    global video;
    global rect;
    global regionMask;
    value = str2double(get(handles.currentFrame, 'String'));
    showFrame(handles, value, video, rect, regionMask)

% --- Executes during object creation, after setting all properties.
function comboFunc_CreateFcn(hObject, ~, ~)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in antBallsCheckbox.
function antBallsCheckbox_Callback(~, ~, ~)
% hObject    handle to antBallsCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of antBallsCheckbox


% --- Executes on button press in FULLSCREEN.
function FULLSCREEN_Callback(~, ~, handles)

    set(handles.axes3,                  'OuterPosition',[-26.875,-2,205.338,55]);%MAIN WINDOW
    set(handles.pushbutton6,            'Position',     [0.6, 3.692, 9.2, 2.231]);%PLAY BUTTON 
    set(handles.pushbutton7,            'Position',     [0.6, 1.231, 9.2, 2.231]);%PLAY BUTTON
    %set(handles.pushbutton1,            'Position',     [0.6, 9.333, 14.4, 2.308]);%PLAY BUTTON

    set(handles.fileName,               'visible', 'off');
    set(handles.selectTargetRegion,     'visible', 'off');
    set(handles.selectPoints,           'visible', 'off');

    set(handles.pushbutton8,            'visible', 'off');
    set(handles.pushbutton9,            'visible', 'off');
    set(handles.pushbutton1,            'visible', 'off');
    set(handles.clearMask,              'visible', 'off');
    set(handles.clearAntButton,         'visible', 'off');
    set(handles.showantpath,            'visible', 'off');
    set(handles.dnaCheck,               'visible', 'off');
    set(handles.boundingBoxesCheck,     'visible', 'off');
    set(handles.OutputAntPath,          'visible', 'off');
    set(handles.slider,                 'visible', 'off');
    set(handles.centroMasa,             'visible', 'off');
    set(handles.masa,                   'visible', 'off');
    set(handles.saveFrame,              'visible', 'off');
    set(handles.seeOtsu,                'visible', 'off');
    set(handles.showantpath,            'visible', 'off');
    set(handles.antBallsCheckbox,       'visible', 'off');
    
    %set(handles.text9,                  'visible', 'off');
    %set(handles.firstFrame,             'visible', 'off');
    %set(handles.text18,                  'visible', 'off');
    %set(handles.text11,                 'visible', 'off');
    %set(handles.pushbutton4,            'visible', 'off');
    %set(handles.pushbutton5,            'visible', 'off');
    set(handles.uipanel5,               'visible', 'off');
    




% --- Executes on button press in NORMALVIEW.
function NORMALVIEW_Callback(~, ~, handles)

    set(handles.axes3,                  'OuterPosition',[-32.875,18.319,205.338,34.547]);%MAIN WINDOW
    set(handles.pushbutton6,            'Position',[20.6, 9.333, 9.2, 2.231]);%PLAY BUTTON 
    set(handles.pushbutton7,            'Position',[29.6, 9.333, 9.2, 2.231]);%PLAY BUTTON
    set(handles.pushbutton1,            'Position',[6, 9.333, 14.4, 2.308]);%PLAY BUTTON

    set(handles.fileName,               'visible', 'on');
    set(handles.selectTargetRegion,     'visible', 'on');
    set(handles.selectPoints,           'visible', 'on');

    set(handles.pushbutton6,            'visible', 'on');
    set(handles.pushbutton7,            'visible', 'on');
    set(handles.pushbutton8,            'visible', 'on');
    set(handles.pushbutton9,            'visible', 'on');
    set(handles.pushbutton1,            'visible', 'on');
    set(handles.clearMask,              'visible', 'on');
    set(handles.clearAntButton,         'visible', 'on');
    set(handles.showantpath,            'visible', 'on');
    set(handles.dnaCheck,               'visible', 'on');
    set(handles.boundingBoxesCheck,     'visible', 'on');
    set(handles.OutputAntPath,          'visible', 'on');
    set(handles.slider,                 'visible', 'on');
    set(handles.centroMasa,             'visible', 'on');
    set(handles.masa,                   'visible', 'on');
    set(handles.saveFrame,              'visible', 'on');
    set(handles.seeOtsu,                'visible', 'on');
    set(handles.showantpath,            'visible', 'on');
    set(handles.antBallsCheckbox,       'visible', 'on');
    set(handles.uipanel5,               'visible', 'on');


% --- Executes on button press in CLEANCONSOLE.
function CLEANCONSOLE_Callback(~, ~, handles)

    global messages;
    global MAX_BUFFER_MESSAGES;
    messages=cell(1,MAX_BUFFER_MESSAGES);
    set(handles.status, 'String', messages );
    drawnow;


% --- Executes on button press in EMPTYPIC.
function EMPTYPIC_Callback(~, ~, handles)

    global PATHS;
    global rect;
    global video;
    global fileName;
    global regionMask;

    currentFrame=get(handles.currentFrame, 'String');
    
    frame = read(video,str2double(currentFrame));
    
    if(size(rect, 2)>0)
        [frame,~]=cropAndBinarize(frame,rect,regionMask);
    end 
        
    filename=[PATHS.VIDEOFRAME,fileName,'empty.png'];
    
    if(~isempty(frame))
        imwrite(frame,filename);
    end




% --- Executes on selection change in comboSpecies.
function comboSpecies_Callback(hObject, eventdata, handles)
% hObject    handle to comboSpecies (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns comboSpecies contents as cell array
%        contents{get(hObject,'Value')} returns selected item from comboSpecies
    
    global PATHS;
    
    combo=handles.comboSpecies;
    active= get(combo,'Value');
    options = get(combo,'String');
    name = options{active};
    
    if(active==1)
        I=imread(strcat(PATHS.IMAGES,'species/','default.jpg'));    
    else
        I=imread([PATHS.IMAGES,'species/',name,'.jpg']);    
    end
        
    imshow(I,'Parent',handles.axes8);
    


% --- Executes during object creation, after setting all properties.
function comboSpecies_CreateFcn(hObject, eventdata, handles)
% hObject    handle to comboSpecies (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
