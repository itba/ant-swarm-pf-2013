{
	"settings": {
        "VIDEOFILES":     "/Users/daniel/Documents/01_PROYECTOS/Hormigas_GrupoInsectosSociales_UBA/Experimentos/VideosSabrina/Avis/",
        "VIDEOSETTINGS":  "output/videoSettings/",
        "VIDEOFRAME":     "output/videoFrames/",
        "VIDEOPROCESS":   "output/videoProcess/mass&centermass/",
        "VIDEOPATHS":     "output/videoProcess/Paths/",
        "VIDEOPARAMETERS":"output/videoProcess/antParameters/",
        "IMAGES":         "images/"
	}, 
    "defaultscan":{
        "BINSHISTOGRAM":    100,
        "LASTFRAME":        2000,
        "LASTFRAMESTEP":    20,
        "MINCLUSTERSIZE":   150,
        "MAXCLUSTERSIZE":   1000,
        "ANTSPECIES":       1,
        "CONFIDENCE":       0.999
    },
    "species":{
        "NAMES":         ["Generic species","Linepithema_humile","Camponotus_mus"]
   }
}