
%Name:          gatherData
%Description:   gather cluster data from last video frame and return an
%               array of cluster ant size
%               It shows an progress bar called waitbar. It is a popup
%Preconditions: video must to be opened before
%Parameters:    video:Video
%               environment:Struct environment constant values

function data=gatherData(video,environment,guivalues)
    
    h=waitbar(0,'Studying ant size ... wait please','CreateCancelBtn',...
            'setappdata(gcbf,''canceling'',1)');
    
    frames     = get(video,'FrameRate');
    duration   = get(video,'Duration');
    
    cantFrames = environment.local.frames(2);
    %cantFrames = frames*duration;
    %cantFrames = cantFrames -1;
    
    if(~exist('guivalues','var') || isempty(guivalues))
        STUDYLASTFRAME      = environment.SCAN.LASTFRAME;
        STEP                = environment.SCAN.LASTFRAMESTEP;
        MIN                 = environment.SCAN.MINCLUSTERSIZE;
        MAX                 = environment.SCAN.MAXCLUSTERSIZE;
    else
        STUDYLASTFRAME      = guivalues.LASTFRAME;
        STEP                = guivalues.LASTFRAMESTEP;
        MIN                 = guivalues.MINCLUSTERSIZE;
        MAX                 = guivalues.MAXCLUSTERSIZE;
    end
    
    rect        = environment.local.rect;
    regionMask  = environment.local.regionMask;
    framesize   = environment.local.framesize;
    
    regionMask =roipoly(framesize(1),framesize(2),regionMask(:,1),regionMask(:,2));
    regionMask =imcrop(regionMask,rect);
    
    % Estimate array size. M�ximun 200 cluster by frame
    
    data=zeros(1,STUDYLASTFRAME/STEP*200);
    i=STUDYLASTFRAME;
    dataindex=1;
    
    for f=cantFrames:-STEP:(cantFrames-STUDYLASTFRAME)
        % Check for Cancel button press
        if getappdata(h,'canceling')
            break
        end
        frame = read(video,f);
        [~,bw]=cropAndBinarize(frame,rect,regionMask);
        CC=bwconncomp(~bw);
        prop=regionprops(CC);
        complete=[prop.Area];
        filtered=[prop.Area]>MIN & [prop.Area]<MAX;
        interest=complete(filtered);
        
        if(size(interest,2))
            data(1,dataindex:dataindex+size(interest,2)-1)=interest;
            dataindex=dataindex+size(interest,2);
        end
        
        if(mod(i,15)==0)
            perc=(STUDYLASTFRAME-i)/STUDYLASTFRAME;
            waitbar(perc,h,sprintf('Studying ant size... wait please %d%%...',round(perc*100)));
        end
        i=i-STEP;
    end
    
    % shrink array
    data=data(1,1:(dataindex-1));
    
    % close waitbar
    delete(h)       % DELETE the waitbar; don't try to CLOSE it.
end