
% Name:             getHistogram
% Parameters:       video:Video         - video object. Must be opened
%                   viewplots:Boolean   - show plots condition
%                   environment:Struct  - environment constants (local,scan,filename,PATHS)

function [antvalues stats]=getHistogram(video, ~, env)
    
    global phandles;

    guivalues={}; % do not take values from gui
    savednormals={};
    tic
        data=gatherData(video,env,guivalues);
        disp('recolectData process');
    toc
    
    openScanParametersGUI();
    
    xlim(phandles.axes1,[env.SCAN.MINCLUSTERSIZE env.SCAN.MAXCLUSTERSIZE]);
    [n,xout]=hist(data,env.SCAN.BINSHISTOGRAM);
    bar(phandles.axes1,xout,n);
    
    xlim(phandles.axes2,[env.SCAN.MINCLUSTERSIZE env.SCAN.MAXCLUSTERSIZE]);
    [antvalues normals]=plotNormals(data,env,guivalues,savednormals);
    stats.normals=normals;
    stats.data=data;
    
end

function openScanParametersGUI()
    mainGUIhandle  = AntScanParameters;       
    guidata(mainGUIhandle);
end
