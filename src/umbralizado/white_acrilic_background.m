function answer = white_acrilic_background( img )
    r=img(:,:,1);
    rev=255-r;

    % regularizar la iluminación
    i2=imtophat(rev,strel('disk',30));
    %i2=rev;
    % equalizar

    i3=imadjust(i2);
    % umbralizar con otsu
    threshold=graythresh(i3);
    i4=im2bw(i3,threshold);
    % primer filtro 
    target= i4>0;
    img(target)=255;
    % segundo filtro con rojo

    target= img(:,:,1)==255 & img(:,:,2)<85 & img(:,:,3)<70;
    answer = zeros(size(i4));
    answer(target) = 1;
    answer = ~answer;

    answer = breakLegs(answer);
    %answer = breakLegs(answer);
  
end

