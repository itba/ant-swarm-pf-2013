function bw = substract_with_empty_eq( img, THRESHOLD )

    global emptyFrame;
   
    if(~isempty(emptyFrame))
        b=img;
        c=emptyFrame-b;
        c=c(:,:,1);
        d=imadjust(c);
        ants=d>THRESHOLD;
        bw=ones(size(c))*255;
        bw(ants)=0;
        bw=im2bw(bw);
    else
        bw=im2bw(img);
    end

end