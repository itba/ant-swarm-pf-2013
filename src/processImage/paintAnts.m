function painted=paintAnts(frame, regionMask, bw)
    
    I=frame;
    Iaux=I;
    if(size(regionMask,1)>0)
        % select white to outside regionMask
        Iaux=frame;
        Iaux(regionMask)=   255 - 3*I(regionMask); % background blue 
        if(size(bw,1)>0)
            otsumask=~bw; 
            Iaux(otsumask)=     255 - 1*I(otsumask);   % red ants
        end
    end
    painted=Iaux;

    

