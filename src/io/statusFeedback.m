function statusFeedback( GUIelement, msg, msg_type )

 global messages;
 global MAX_BUFFER_MESSAGES;
 
%  heigth=get(handles.status,'Position');
%  heigth=heigth(2);
%  disp(heigth/10);

 index=MAX_BUFFER_MESSAGES-sum(cellfun(@isempty,messages))+1;
 
 if(index>MAX_BUFFER_MESSAGES)
     messages=messages(1,2:(size(messages,2)));
     index=MAX_BUFFER_MESSAGES;
 end
 
 messages{1,index}=msg;
 
 set(GUIelement, 'Visible', 'on' );
 set(GUIelement, 'String', messages );
 set(GUIelement, 'BackgroundColor', [1,1,0]);
 drawnow;

end

