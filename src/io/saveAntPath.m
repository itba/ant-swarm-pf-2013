function saveAntPath(path,fileName,ants,lastAnts)

    % 1) armo una estructura para cada ants y lastAnts con el frame que
    %corresponda
    % 2) persisto el archivo con la información
    
    if((size(ants,2)+size(lastAnts,2))>0)
        
        pathsize=0;
        for i=1:size(ants,2)
            pathsize=pathsize+size(ants{i}.path,2);
        end
        for i=1:size(lastAnts,2)
            for j=1:size(lastAnts{i},2)
                pathsize=pathsize+size(lastAnts{i}{j}.path,2);
            end
        end
        paths=zeros(pathsize,5);
        k=1;
         
%       tic
         [k, paths]=buildPath(k,paths,ants);
         [~, paths]=buildPathFIX(k,paths,lastAnts);
%          disp('Tiempo en Armar las estructuras para grabar');
%        toc
       
%        tic  
        dlmwrite([path,fileName,'.path.txt'],paths);
%         disp('Tiempo en bajar a archivo');
%        toc
    end
end

% [Ant id,frame, x-position, y-position, ant area]

function [t newPath]=buildPath(k,paths,set)
    for i=1:size(set,2)
         id=set{i}.id;
         frame=set{i}.firstFrame;
         for j=1: size(set{i}.path,2)
             step=set{i}.path{j};
             paths(k,:)=[id,frame,step(1),step(2),set{i}.region.Area];
             frame=frame+1;
             k=k+1;
         end
    end
     t=k;
     newPath=paths;
end


function [t newPath]=buildPathFIX(k,paths,set)
    for i=1:size(set,2)
        for w=1:size(set{i},2)
             id=set{i}{w}.id;
             frame=set{i}{w}.firstFrame;
             for j=1: size(set{i}{w}.path,2)
                 step=set{i}{w}.path{j};
                 paths(k,:)=[id,frame,step(1),step(2),set{i}{w}.region.Area];
                 frame=frame+1;
                 k=k+1;
             end
        end
    end
     t=k;
     newPath=paths;
end
