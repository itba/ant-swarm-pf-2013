function [antvalues normals]=plotNormals(data, env, guivalues,savednormals)

    global phandles;
        
    if(~exist('guivalues','var') || isempty(guivalues))
        CONFIDENCE  =env.SCAN.CONFIDENCE;
        SPECIES     =env.SCAN.ANTSPECIES;
        MIN         =env.SCAN.MINCLUSTERSIZE;
        MAX         =env.SCAN.MAXCLUSTERSIZE;
    else
        CONFIDENCE  =guivalues.CONFIDENCE;
        SPECIES     =guivalues.ANTSPECIES;
        MIN         =guivalues.MINCLUSTERSIZE;
        MAX         =guivalues.MAXCLUSTERSIZE;
    end
    
    %[em_thr,em_thr_behavior,P,meanV,stdV,pdf_x,xx,pdf_xx,cdf_xx] ... 
    %   = em_1dim(data, GAUSSIANANT);

    if(~exist('savednormals','var')|| isempty(savednormals))
        [~,~,~,meanV,stdV,~,xx,~,~]= em_1dim(data, SPECIES);
    else
        meanV   = savednormals.meanV;
        stdV    = savednormals.stdV;
        xx      = savednormals.xx;
    end
    
    cla(phandles.axes2);
    xlim(phandles.axes2,[MIN MAX]);
    
    c={'r','g','b','c','m','y','k'};
    
    for p=1:SPECIES
        plot(phandles.axes2, xx, normpdf(xx, meanV(p), stdV(p)), c{p});
        hold on;
    end
    
    [~, p]=max(normpdf(meanV,meanV,stdV));
    
    axes(phandles.axes2);
    
    text(meanV(p),normpdf(meanV(p), meanV(p), stdV(p)),...
        '\leftarrow Ant','HorizontalAlignment','left')
    
    text(meanV(p),normpdf(meanV(p), meanV(p), stdV(p))/2,...
        num2str(CONFIDENCE),'HorizontalAlignment','left')
    
    hold on;
    
    alpha=1-CONFIDENCE;
    i1=norminv(alpha,meanV(p), stdV(p));
    i2=norminv(1-alpha,meanV(p), stdV(p));
     xRaya=meanV(p);

    % intervalos de confianza
    plot(phandles.axes2, i1,normpdf(i1, meanV(p), stdV(p)),'o',i2,...
    normpdf(i2, meanV(p), stdV(p)),'s');
    hold on; 
    
    antvalues.mean  =xRaya;
    antvalues.min   =i1;
    antvalues.max   =i2;
    
    normals.meanV=meanV;
    normals.stdV=stdV;
    normals.xx=xx;

end