function printAntBalls( fullPathfilename, antBalls )
    try
        current = pwd;
        fidAntBalls = fopen(strcat(fullPathfilename),'w+');
        cd(current);

        [~, c] = size(antBalls);



        for i=1 : c
            %frame, min, max, mean
            %fprintf(fidAntBalls, '%d, %f, %f, %f\r\n', antBalls{i}{1}, ...
            %    min([antBalls{i}{2}.Area]), max([antBalls{i}{2}.Area]), mean([antBalls{i}{2}.Area]));
            [balls ~] = size(antBalls{i}{2}.ball);
            for j=1 : balls
                %iAnts=round(antBalls{i}{2}(j).Area/CURRENT_MEAN_ANT);
                %Frame, X, Y, Area
                fprintf(fidAntBalls,'%d, %.3f, %.3f, %i, %d\n', antBalls{i}{1}, antBalls{i}{2}.ball(j).Centroid, [antBalls{i}{2}.ball(j).Area], antBalls{i}{2}.iAnt(j));
            end
        end

        fclose(fidAntBalls);

    catch EM
        % user cancel
    end

    %[r c] = size(antBalls);

    %for i=1:c
    %    frame = antBalls{i}{1};
    %    [balls noimporta] = size(antBalls{i}{2});
    %    fprintf('frame: %i\n', frame);
    %    fprintf('Min: %i\n', min([antBalls{i}{2}.Area]));
    %    fprintf('Max: %i\n', max([antBalls{i}{2}.Area]));
    %    fprintf('Mean: %i\n', mean([antBalls{i}{2}.Area]));
        
    %    for j=1 : balls
    %        fprintf('     Area: %i\n', [antBalls{i}{2}(j).Area]);
    %        fprintf('     CENTRO ID: (%i,%i)\n', antBalls{i}{2}(j).Centroid);
    %    end
    %end

