

function selected=selectAnts(bw,ants,currentFrame,antvalues, handles)

    [~, regions, ~]=countAnts(bw,antvalues, handles);
    ANT_PROXIMITY=round(0.08*antvalues.mean);   
    MAX_INT=intmax('uint32');
    try
        [px,py]=getpts(handles.axes3);              % select points from interface
    catch EM
        statusFeedback(handles.status,'selection canceled');       
    end
    
    if(exist('ants','var') && size(ants,2)>0)
        selected=ants;          % initialize ants set with previus ant selection
    else
        selected={};            % initialize ant selected set
    end
    
    old=size(selected,2);
    aof=0;                      % point far of ant
    
    % 1) search for nearest region ant to selected points
    
    for j=1:size(px,1)
        min=MAX_INT; % distance between 2 points
        
        for i=1:size(regions,1)
            % euclidean distance
            distance=norm([px(j),py(j)]-regions(i).Centroid);
            
            if(distance < ANT_PROXIMITY && distance < min)
                min=distance;                           % auxiliar
                selected{old+j-aof}.region=regions(i);  % region properties
                selected{old+j-aof}.id=old+j-aof;       % ant id
            end
        end
        if(min==MAX_INT)
            aof=aof+1;
        else
            % 2)initialize selected.path with first ant mass center
            selected{old+j-aof}.path={};
            selected{old+j-aof}.path{1}=round(selected{old+j-aof}.region.Centroid);
            selected{old+j-aof}.firstFrame=currentFrame;
            selected{old+j-aof}.tracked=1;
        end
        
    end
   
    % 3)display selected ant bounding boxes with their id number
    
    for k=1:size(selected,2)
        bc =selected{k}.region.Centroid;        
        rectangle('Position',selected{k}.region.BoundingBox,'EdgeColor','g','LineWidth',2,'Curvature',[1,1],'LineStyle','--');
        a=text(bc(1)+15,bc(2), int2str(k));
        set(a, 'FontName', 'Arial', 'FontWeight', 'bold', 'FontSize', 12, 'Color', 'yellow');
    end
    
end
    