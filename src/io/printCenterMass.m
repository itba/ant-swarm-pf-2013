function printCenterMass( fullPathfilename, centerMassStruct)

current = pwd;
try
    fidCentroMasa = fopen(strcat(fullPathfilename),'w+');
    cd(current);

    [~, c] = size(centerMassStruct);

    for i=1 : c
        fprintf(fidCentroMasa, '%d, %f, %f\r\n', centerMassStruct{i}{1}, ...
            centerMassStruct{i}{2}(1), centerMassStruct{i}{2}(2));
    end

    fclose(fidCentroMasa);
catch EM
    % user cancel
end