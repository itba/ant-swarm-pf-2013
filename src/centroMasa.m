function [ answer ] = centroMasa( logicalImage )
%CENTROMASA Summary of this function goes here
%   Devuelve el centroid de la imagen

    aux = +logicalImage;
    aux = 1-aux;
    answer = regionprops(aux, 'Centroid');

end

